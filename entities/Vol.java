package com.example.gestionvols.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Vol {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String numeroVol;
    private String aeroportDepart;
    private String aeroportArrivee;
    
	public Vol() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Vol(Long id, String numeroVol, String aeroportDepart, String aeroportArrivee) {
		super();
		this.id = id;
		this.numeroVol = numeroVol;
		this.aeroportDepart = aeroportDepart;
		this.aeroportArrivee = aeroportArrivee;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroVol() {
		return numeroVol;
	}

	public void setNumeroVol(String numeroVol) {
		this.numeroVol = numeroVol;
	}

	public String getAeroportDepart() {
		return aeroportDepart;
	}

	public void setAeroportDepart(String aeroportDepart) {
		this.aeroportDepart = aeroportDepart;
	}

	public String getAeroportArrivee() {
		return aeroportArrivee;
	}

	public void setAeroportArrivee(String aeroportArrivee) {
		this.aeroportArrivee = aeroportArrivee;
	}
    
}
