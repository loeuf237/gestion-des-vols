package com.example.gestionvols.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Avion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String numeroSerie;
    private String compagnie;
    
    
	public Avion() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Avion(Long id, String numeroSerie, String compagnie) {
		super();
		this.id = id;
		this.numeroSerie = numeroSerie;
		this.compagnie = compagnie;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNumeroSerie() {
		return numeroSerie;
	}


	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}


	public String getCompagnie() {
		return compagnie;
	}


	public void setCompagnie(String compagnie) {
		this.compagnie = compagnie;
	}
    
}
