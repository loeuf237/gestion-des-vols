package com.example.gestionvols.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToOne
    private Vol vol;
    
    @ManyToOne
    private Passager passager;
    // Autres attributs
    
    // Constructeurs, getters, setters, etc.
}

